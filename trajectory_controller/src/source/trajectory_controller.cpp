#include "trajectory_controller.h"

//Constructor
TrajectoryController::TrajectoryController()
{
    std::cout << "Constructor: TrajectoryController" << std::endl;

}

//Destructor
TrajectoryController::~TrajectoryController() {}


void TrajectoryController::Initialize()
{
    calculated = false;
}


bool TrajectoryController::readConfigs(std::string configFile)
{

    try
    {

    XMLFileReader my_xml_reader(configFile);

    /*********************************  Constant Needed ************************************************/

    v_maxxy = my_xml_reader.readDoubleValue("Trajectory_Controller:Constants:Vmaxxy");
    v_maxz = my_xml_reader.readDoubleValue("Trajectory_Controller:Constants:Vmaxz");
    t_90 = my_xml_reader.readDoubleValue("Trajectory_Controller:Constants:t_90");

    /*********************************  PID Parameters ************************************************/

    pid_pitch_kp = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Pitch:Gain:Kp");
    pid_pitch_ki = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Pitch:Gain:Ki");
    pid_pitch_kd = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Pitch:Gain:Kd");
    pid_pitch_enablesat = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Pitch:Saturation:enable_saturation");
    pid_pitch_satmax = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Pitch:Saturation:SatMax");
    pid_pitch_satmin = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Pitch:Saturation:SatMin");
    pid_pitch_enableantiwp = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Pitch:Anti_wind_up:enable_anti_wind_up");
    pid_pitch_kw = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Pitch:Anti_wind_up:Kw");

    pid_roll_kp = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Roll:Gain:Kp");
    pid_roll_ki = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Roll:Gain:Ki");
    pid_roll_kd = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Roll:Gain:Kd");
    pid_roll_enablesat = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Roll:Saturation:enable_saturation");
    pid_roll_satmax = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Roll:Saturation:SatMax");
    pid_roll_satmin = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Roll:Saturation:SatMin");
    pid_roll_enableantiwp = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Roll:Anti_wind_up:enable_anti_wind_up");
    pid_roll_kw = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Roll:Anti_wind_up:Kw");

    pid_dx_kp = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Dx:Gain:Kp");
    pid_dx_ki = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Dx:Gain:Ki");
    pid_dx_kd = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Dx:Gain:Kd");
    pid_dx_enablesat = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Dx:Saturation:enable_saturation");
    pid_dx_satmax = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Dx:Saturation:SatMax");
    pid_dx_satmin = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Dx:Saturation:SatMin");
    pid_dx_enableantiwp = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Dx:Anti_wind_up:enable_anti_wind_up");
    pid_dx_kw = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Dx:Anti_wind_up:Kw");

    pid_dy_kp = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Dy:Gain:Kp");
    pid_dy_ki = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Dy:Gain:Ki");
    pid_dy_kd = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Dy:Gain:Kd");
    pid_dy_enablesat = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Dy:Saturation:enable_saturation");
    pid_dy_satmax = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Dy:Saturation:SatMax");
    pid_dy_satmin = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Dy:Saturation:SatMin");
    pid_dy_enableantiwp = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Dy:Anti_wind_up:enable_anti_wind_up");
    pid_dy_kw = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_Dy:Anti_wind_up:Kw");

    pid_h_kp = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_H:Gain:Kp");
    pid_h_ki = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_H:Gain:Ki");
    pid_h_kd = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_H:Gain:Kd");
    pid_h_enablesat = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_H:Saturation:enable_saturation");
    pid_h_satmax = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_H:Saturation:SatMax");
    pid_h_satmin = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_H:Saturation:SatMin");
    pid_h_enableantiwp = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_H:Anti_wind_up:enable_anti_wind_up");
    pid_h_kw = my_xml_reader.readDoubleValue("Trajectory_Controller:PID_H:Anti_wind_up:Kw");

    }


    catch ( cvg_XMLFileReader_exception &e)
    {
        throw cvg_XMLFileReader_exception(std::string("[cvg_XMLFileReader_exception! caller_function: ") + BOOST_CURRENT_FUNCTION + e.what() + "]\n");
    }

    return true;

}

void TrajectoryController::setUp()
{
    Initialize();

    ros::param::get("~stackPath", stackPath);
    if ( stackPath.length() == 0)
    {
        stackPath = "$(env AEROSTACK_STACK)";
    }
    ros::param::get("~droneId", idDrone);
    ros::param::get("~trj_config_file", trjconfigFile);
    if ( trjconfigFile.length() == 0)
    {
        trjconfigFile="trajectory_controller.xml";
    }

    bool readConfigsBool = readConfigs(stackPath+"/configs/drone"+cvg_int_to_string(idDrone)+"/"+trjconfigFile);

    if(!readConfigsBool)
    {
        std::cout << "Error init"<< std::endl;
        return;
    }

    std::cout << "Constructor: TrajectoryController...Exit" << std::endl;


}

void TrajectoryController::start()
{

//    // Publisher
    pub_posref = (this->nIn).advertise<droneMsgsROS::dronePositionRefCommandStamped>("dronePositionRefs", 1,this);

    pub_yawcmd = (this->nIn).advertise<droneMsgsROS::droneYawRefCommand>("droneControllerYawRefCommand", 1,this);

    PID_Pitch.reset();
    PID_Pitch.setGains(pid_pitch_kp,pid_pitch_ki,pid_pitch_kd);
    PID_Pitch.enableMaxOutput(pid_pitch_enablesat,pid_pitch_satmin,pid_pitch_satmax);
    PID_Pitch.enableAntiWindup(pid_pitch_enableantiwp,pid_pitch_kw);
    PID_Roll.reset();
    PID_Roll.setGains(pid_roll_kp,pid_roll_ki,pid_roll_kd);
    PID_Roll.enableMaxOutput(pid_roll_enablesat,pid_roll_satmin,pid_roll_satmax);
    PID_Roll.enableAntiWindup(pid_roll_enableantiwp,pid_roll_kw);

    PID_Dx.reset();
    PID_Dx.setGains(pid_dx_kp,pid_dx_ki,pid_dx_kd);
    PID_Dx.enableMaxOutput(pid_dx_enablesat,pid_dx_satmin,pid_dx_satmax);
    PID_Dx.enableAntiWindup(pid_dx_enableantiwp,pid_dx_kw);
    PID_Dy.reset();
    PID_Dy.setGains(pid_dy_kp,pid_dy_ki,pid_dy_kd);
    PID_Dy.enableMaxOutput(pid_dy_enablesat,pid_dy_satmin,pid_dy_satmax);
    PID_Dy.enableAntiWindup(pid_dy_enableantiwp,pid_dy_kw);
    PID_H.reset();
    PID_H.setGains(pid_h_kp,pid_h_ki,pid_h_kd);
    PID_H.enableMaxOutput(pid_h_enablesat,pid_h_satmin,pid_h_satmax);
    PID_H.enableAntiWindup(pid_h_enableantiwp,pid_h_kw);

    iterator = 0;
    straigh_end = false;
    calculated = false;
    raise_end = false;
    heading_over = false;
    last_waypoint = false;
    start_done = false;
    turn_end = true;

}


void TrajectoryController::stop()
{
}

void TrajectoryController::straigh()
{

    float distance = (abs((actual_point.y-last_point.y)*x - (actual_point.x-last_point.x)*y + actual_point.x*last_point.y - actual_point.y*last_point.x)/(sqrt(pow(actual_point.y-last_point.y,2)+pow(actual_point.x-last_point.x ,2))));       //distance point to line

    if ((((x-last_point.x)*(actual_point.y-last_point.y))-((y-last_point.y)*(actual_point.x-last_point.x))) > 0) distance = -1*distance;

    command(distance, vxy);

}

void TrajectoryController::turn()
{
    yaw_ref = next_yaw;
    error = yaw_ref-yaw;
    if (error >= M_PI){
        error -= 2*M_PI;
    }else if (error <= -M_PI){
        error += 2*M_PI;
    }
    if (abs(error)>0.05){
        float yaw_actual=yaw*(M_PI/2)/(abs(yaw_ref - actual_yaw));
        velx = vxy/10;
        velz= vz/2;

        command(sin(yaw_actual)*velx+cos(yaw_actual)*next_vxy/10, cos(yaw_actual)*velx+sin(yaw_actual)*next_vxy/10);
    } else{
        iterator++;
        calculated = false;
        turn_end = true;
        PID_Dx.reset();
        PID_Dy.reset();
        PID_H.reset();
        straigh_end = false;
        raise_end = false;
        if (iterator >= number_point){
            if (trajectory.is_periodic) iterator = 0;

        }
    }
}

void TrajectoryController::command(float yb, float v_x)
{


    float dxb =  dx * cos (yaw) + dy * sin (yaw);
    float dyb  =  - dx * sin (yaw) + dy * cos (yaw);

    /*********************************  Speed X Controller ( from Dx to Pitch ) ************************************************/

    PID_Pitch.setReference(v_x);
    PID_Pitch.setFeedback(dxb);
    pitch_ref = -PID_Pitch.getOutput();


    /*********************************  Speed Y Controller ( from Dy to Roll ) **************************************************/

    PID_Roll.setReference(0.0);
    PID_Roll.setFeedback(yb);
    roll_ref = - PID_Roll.getOutput();

}


void TrajectoryController::setReference(droneMsgsROS::dronePositionTrajectoryRefCommand trajectory_ref){
    //! Check if there are points on trajectory  checking size??
    if (trajectory_ref.droneTrajectory.size()>0){
        trajectory = trajectory_ref;
        number_point = trajectory_ref.droneTrajectory.size();
        initial_checkpoint = trajectory.initial_checkpoint;
        if (initial_checkpoint != 0){
            trajectory.droneTrajectory.clear();
            std::vector<droneMsgsROS::dronePositionRefCommand>::const_iterator it_mod = (trajectory_ref.droneTrajectory).begin();
            for (std::vector<droneMsgsROS::dronePositionRefCommand>::const_iterator it = (trajectory_ref.droneTrajectory).begin();
                 it != (trajectory_ref.droneTrajectory).end();
                 ++it) {

                if (it + initial_checkpoint - (trajectory_ref.droneTrajectory).begin() >= number_point && !start_done) {

                    it_mod = (trajectory_ref.droneTrajectory).begin();
                    start_done = true;

                } else if (it + initial_checkpoint - (trajectory_ref.droneTrajectory).begin() >= number_point&& start_done){

                    it_mod++;

                } else {

                    it_mod = it + initial_checkpoint;

                }

                droneMsgsROS::dronePositionRefCommand next_waypoint;
                next_waypoint.x = it_mod->x;
                next_waypoint.y = it_mod->y;
                next_waypoint.z = it_mod->z;
                trajectory.droneTrajectory.push_back(next_waypoint);
                std::cout<<next_waypoint<<std::endl;
            }

            trajectory.initial_checkpoint = 0;
        }

    }
    calculated = false;
    iterator = 0;

}

void TrajectoryController::setFeedback(droneMsgsROS::dronePose last_Pose, droneMsgsROS::droneSpeeds last_Speed){

    x = last_Pose.x;
    y = last_Pose.y;
    z = last_Pose.z;
    roll = last_Pose.roll;
    pitch = last_Pose.pitch;
    yaw = last_Pose.yaw;
    dx = last_Speed.dx;
    dy = last_Speed.dy;

}

void TrajectoryController::getOutput(float *pitch, float *roll, float *dz, float *yaw_des, bool *trj_end){

    if (iterator < number_point){


        if (turn_end && !calculated ){
            setMove();
        }

        if ((sqrt(pow(actual_point.x-x,2)+pow(actual_point.y-y,2)))>radius && !straigh_end && turn_end)  {
            straigh();
            *trj_end = false;
        }
        else if ((sqrt(pow(actual_point.x-x,2)+pow(actual_point.y-y,2)))>radius/2  && turn_end){
            straigh_end = true;

            PID_Dx.setReference(0.0);
            PID_Dx.setFeedback(dx);
            float pitchw = - PID_Dx.getOutput();
            PID_Dy.setReference(0.0);
            PID_Dy.setFeedback(dy);
            float rollw = PID_Dy.getOutput();
            pitch_ref =  pitchw * cos (yaw) - rollw * sin (yaw);
            roll_ref  =  - pitchw * sin (yaw) - rollw * cos (yaw);

        }else straigh_end = true;

        if ((abs(actual_point.z-z) > 0.20) && !raise_end  && turn_end){

            velz = vz;
            *trj_end = false;
        }else if (!straigh_end && (abs(actual_point.z-z)) > 0.10  && turn_end){
            raise_end = true;
            PID_H.setReference(actual_point.z);
            PID_H.setFeedback(z);
            velz = PID_H.getOutput();
        }else raise_end = true;


        if (straigh_end){
            turn_end = false;
            turn();
            *trj_end = false;
        }
        error = yaw_ref-yaw;
        if ( (iterator == 0) && !heading_over){
            if (error >= M_PI){
                error -= 2*M_PI;
            }else if (error <= -M_PI){
                error += 2*M_PI;
            }
            if (abs(error)>0.05){
                *dz = 0.0;
                *pitch = 0.0;
                *roll = 0.0;
                PID_Dx.reset();
                PID_Dy.reset();
                PID_H.reset();
                PID_Pitch.reset();
                PID_Roll.reset();
            } else{
                heading_over = true;
                *dz = velz;
                *pitch = pitch_ref;
                *roll = roll_ref;
            }
        }else{
            heading_over = true;
            *dz = velz;
            *pitch = pitch_ref;
            *roll = roll_ref;
        }
        *yaw_des = yaw_ref;

    }
    else{
        *dz = 0.0;
        *pitch = 0.0;
        *roll = 0.0;
        *yaw_des = 0.0;
        *trj_end = true;
    }


}

void TrajectoryController::setMove(){


    if (!calculated){
        if (iterator == 0){
            last_point.x = x;
            last_point.y = y;
            last_point.z = z;
        }else last_point = actual_point;



        actual_point = trajectory.droneTrajectory[iterator];

        droneMsgsROS::dronePositionRefCommandStamped  drone_position_reference;
        drone_position_reference.position_command.x = actual_point.x;
        drone_position_reference.position_command.y = actual_point.y;
        drone_position_reference.position_command.z = actual_point.z;
        pub_posref.publish(drone_position_reference);



        if ((actual_point.x-last_point.x) != 0.0){
                actual_yaw = + atan((actual_point.y-last_point.y)/(actual_point.x-last_point.x));
                if ((actual_point.x-last_point.x) < 0.0) {
                    actual_yaw -= M_PI;

                }
        }else{
            if ((actual_point.y-last_point.y) > 0) actual_yaw = +M_PI/2;
            else if ((actual_point.y-last_point.y) < 0) actual_yaw = -M_PI/2;
            else actual_yaw = 0;
        }
        if (actual_yaw > M_PI){
            actual_yaw -= 2 * M_PI;
        }
        else if(actual_yaw < -M_PI){
            actual_yaw += 2 * M_PI;
        }
        yaw_ref = actual_yaw;




        droneMsgsROS::droneYawRefCommand drone_yaw_reference;
        drone_yaw_reference.yaw = yaw_ref;

        pub_yawcmd.publish(drone_yaw_reference);

        if (iterator+1 >= number_point) {
            if (trajectory.is_periodic){
                next_point = trajectory.droneTrajectory[0];
            }
            else{
                last_waypoint = true;
                next_point = actual_point;
            }
        }
        else next_point = trajectory.droneTrajectory[iterator+1];


        distxy = sqrt(pow(actual_point.x-last_point.x,2)+pow(actual_point.y-last_point.y,2));
        signdistz = (actual_point.z-last_point.z);
        distz = abs(signdistz);

        next_distxy = sqrt(pow(next_point.x-actual_point.x,2)+pow(next_point.y-actual_point.y,2));
        signnext_distz = (next_point.z-actual_point.z);
        next_distz = abs(signnext_distz);


        if ((next_point.x == actual_point.x)&&(next_point.y == actual_point.y)&&(next_point.z == actual_point.z)) next_yaw = actual_yaw;
        else {
            if ((next_point.x-actual_point.x) != 0.0){
                    next_yaw = + atan((next_point.y-actual_point.y)/(next_point.x-actual_point.x));
                    if ((next_point.x-actual_point.x) < 0.0) next_yaw -= M_PI;

            }else{
                if ((next_point.y-actual_point.y) > 0) next_yaw = +M_PI/2;
                else if ((next_point.y-actual_point.y) < 0) next_yaw = -M_PI/2;
                else next_yaw = 0;
            }
            if (next_yaw > M_PI){
                next_yaw -= 2 * M_PI;
            }
            else if(next_yaw < -M_PI){
                next_yaw += 2 * M_PI;
            }
        }

        if (actual_yaw - next_yaw < M_PI) next_yaw -= 2*M_PI;
        if (actual_yaw - next_yaw > M_PI) next_yaw += 2*M_PI;     

        t_turn = abs(t_90 * (next_yaw - yaw_ref))/(M_PI/2);

        if ((distxy/v_maxxy)>=(distz/v_maxz)) t =  (distxy/v_maxxy);
        else t = (distz/v_maxz);
        if (t != 0){
            vxy = distxy/t;
            vz = signdistz/t;
        }else {
            vz = 0;
            vxy = 0;
        }

        velz = vz;

        if ((next_distxy/v_maxxy)>=(next_distz/v_maxz)){
            if (next_distxy != 0) {
                next_vz = signnext_distz*v_maxxy/next_distxy;
                next_vxy = v_maxxy;
            }
            else {
                next_vz = 0.0;
                next_vxy = 0.0;
            }


        }
        else{
            if (next_distz != 0) {
                next_vxy = next_distxy*v_maxz/next_distz;
                next_vz = v_maxz;
            }
            else {
                next_vz = 0.0;
                next_vxy = 0.0;
            }

        }

        if (last_waypoint) radius = 1.0;
        else {
            radius = vxy/4*t_turn;
            if (radius < 1.0) radius = 1.0;
        }

        if ((radius > distxy)&&(t != 0)){
            t_turn = t/2;
        }

        calculated = true;
        straigh_end = false;
        raise_end = false;
    }
}
