#ifndef trajectory_controller
#define trajectory_controller

#include "pid_control.h"

/*!*************************************************************************************
 *  \class     trajectory_controller
 *
 *  \brief     Trajectory Controller
 *
 *  \details   This class is in charge of control a trajectory using PID class.
 *             Sends as Output Commands to MidLevel Controller.
 *
 *
 *  \authors   Pablo Santofimia Ruiz
 *
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ***************************************************************************************/

#include <ros/ros.h>


#include "droneMsgsROS/dronePositionRefCommandStamped.h"

#include "droneMsgsROS/dronePose.h"

#include "droneMsgsROS/droneYawRefCommand.h"

#include "droneMsgsROS/droneSpeeds.h"

#include "droneMsgsROS/dronePositionTrajectoryRefCommand.h"

#include "xmlfilereader.h"

#include "math.h"


class TrajectoryController
{
public:

  void setUp();

  void start();

  void stop();

  void Initialize();

  void straigh();
  void turn();
  void setReference(droneMsgsROS::dronePositionTrajectoryRefCommand trajectory_ref);
  void setFeedback(droneMsgsROS::dronePose last_Pose, droneMsgsROS::droneSpeeds last_Speed);
  void command(float y_desired, float v_x);
  void setMove();

  void getOutput(float *pitch, float *roll, float *dz, float *yaw_des, bool *trj_end);

  //! Read Config
  bool readConfigs(std::string configFile);

  //! Constructor. \details Same arguments as the ros::init function.
  TrajectoryController();

  //! Destructor.
  ~TrajectoryController();

private:


   PID PID_Pitch;
   PID PID_Roll;

   PID PID_Dx; // 3 PIDs to maintain position in case it is necessary
   PID PID_Dy;
   PID PID_H;


  //! ROS NodeHandler used to manage the ROS communication
  ros::NodeHandle nIn;

  //! Configuration file variable
  int idDrone;               // Id Drone integer (number of the drone)
  std::string trjconfigFile;    // Config File String name
  std::string stackPath;     // Config File String aerostack path name


  //! ROS publisher
  ros::Publisher pub_posref;
  ros::Publisher pub_yawcmd;


  // Position
  float x;
  float y;
  float z;

  // Speed Commanded
  float dx;
  float dy;

  // Constants
  float v_maxxy;
  float v_maxz;
  float t_90;


  //PIDs for Speed and Roll Correction

  float pid_pitch_kp;
  float pid_pitch_ki;
  float pid_pitch_kd;
  bool pid_pitch_enablesat;
  float pid_pitch_satmax;
  float pid_pitch_satmin;
  bool pid_pitch_enableantiwp;
  float pid_pitch_kw;

  float pid_roll_kp;
  float pid_roll_ki;
  float pid_roll_kd;
  bool pid_roll_enablesat;
  float pid_roll_satmax;
  float pid_roll_satmin;
  bool pid_roll_enableantiwp;
  float pid_roll_kw;

  float pid_dx_kp;
  float pid_dx_ki;
  float pid_dx_kd;
  bool pid_dx_enablesat;
  float pid_dx_satmax;
  float pid_dx_satmin;
  bool pid_dx_enableantiwp;
  float pid_dx_kw;

  float pid_dy_kp;
  float pid_dy_ki;
  float pid_dy_kd;
  bool pid_dy_enablesat;
  float pid_dy_satmax;
  float pid_dy_satmin;
  bool pid_dy_enableantiwp;
  float pid_dy_kw;

  float pid_h_kp;
  float pid_h_ki;
  float pid_h_kd;
  bool pid_h_enablesat;
  float pid_h_satmax;
  float pid_h_satmin;
  bool pid_h_enableantiwp;
  float pid_h_kw;

  float pitch_ref;
  float roll_ref;
  float pitch;
  float roll;

  // Trajectory
  droneMsgsROS::dronePositionTrajectoryRefCommand trajectory;
  int initial_checkpoint;
  droneMsgsROS::dronePositionRefCommand last_point;
  droneMsgsROS::dronePositionRefCommand actual_point;
  droneMsgsROS::dronePositionRefCommand next_point;
  float number_point;
  int iterator;

  bool calculated;
  bool straigh_end;
  bool raise_end;
  bool turn_end;
  bool heading_over;
  bool last_waypoint;
  bool start_done;
  float yaw;
  float next_yaw;
  float error;
  float yaw_ref;
  float actual_yaw;


  float distxy;
  float distz;
  float signdistz;
  float next_distxy;
  float next_distz;
  float signnext_distz;
  float t_turn;
  float t;
  float vxy;
  float vz;
  float next_vxy;
  float next_vz;
  float radius;
  float velx;
  float velz;

  bool Start_Stop_Trajectory_Controller;

};
#endif 
